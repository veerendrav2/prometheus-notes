# Prometheus Installation

* Download: https://prometheus.io/download/

1. Create user and dictionary
```
$ sudo useradd -M -r -s /bin/false prometheus
$ sudo mkdir /etc/prometheus /var/lib/prometheus
```
2. Download pre-compiled binaries from [here](https://prometheus.io/download/)
```
$ wget https://github.com/prometheus/prometheus/releases/download/v2.16.0/prometheus-2.16.0.linux-amd64.tar.gz
$ tar xzf prometheus-2.16.0.linux-amd64.tar.gz prometheus-2.16.0.linux-amd64/
```
3. Move files and set ownership
```
$ sudo cp prometheus-2.16.0.linux-amd64/{prometheus,promtool} /usr/local/bin/
$ sudo chown prometheus:prometheus /usr/local/bin/{prometheus,promtool}
$ sudo cp -r prometheus-2.16.0.linux-amd64/{consoles,console_libraries} /etc/prometheus/
$ sudo cp prometheus-2.16.0.linux-amd64/prometheus.yml /etc/prometheus/prometheus.yml
$ sudo chown -R prometheus:prometheus /etc/prometheus
$ sudo chown prometheus:prometheus /var/lib/prometheus
```
Test the setup
```
$ prometheus --config.file=/etc/prometheus/prometheus.yml
```
4. Create systemd unit file
```
[Unit]
Description=Prometheus Time Series Collection and Processing Server
Wants=network-online.target
After=network-online.target

[Service]
User=prometheus
Group=prometheus
Type=simple
ExecStart=/usr/local/bin/prometheus \
    --config.file /etc/prometheus/prometheus.yml \
    --storage.tsdb.path /var/lib/prometheus/ \
    --web.console.templates=/etc/prometheus/consoles \
    --web.console.libraries=/etc/prometheus/console_libraries

[Install]
WantedBy=multi-user.target
```
5. Start daemon
```
$ sudo systemctl daemon-reload
$ sudo systemctl start prometheus
$ sudo systemctl enable prometheus
```
Test
```
curl localhost:9090
```

# Node Exporter

* Create user for node exporter
```
$ sudo useradd -M -r -s /bin/false node_exporter
```
* Download node_exporter binary from [releases page](https://github.com/prometheus/node_exporter/releases)
* Copy binary to /usr/local/bin
```
sudo cp node_exporter-0.18.1.linux-amd64/node_exporter /usr/local/bin/
sudo chown node_exporter:node_exporter /usr/local/bin/node_exporter
```
* Create systemd unit file
```
sudo vi /etc/systemd/system/node_exporter.service
```
* Start node_exporter daemon
```
$ sudo systemctl daemon-reload
$ sudo systemctl start node_exporter
$ sudo systemctl enable node_exporter
```

