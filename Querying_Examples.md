# Querying
Run a simple query using a time-series selector:
```
node_cpu_seconds_total
```
Use a time-series selector with a label:
```
node_cpu_seconds_total{cpu="0"}
```
Run some queries to experiment with various types of label matching:
```
node_cpu_seconds_total{cpu="0"}

node_cpu_seconds_total{cpu!="0"}

node_cpu_seconds_total{mode=~"s.*"}

node_cpu_seconds_total{mode=~"user|system"}

node_cpu_seconds_total{mode!~"user|system"}
```

Use a range vector selector to select time-series values over a period of two minutes:
```
node_cpu_seconds_total{cpu="0"}[2m]
```

Select data from the past using an offset modifier:
```
node_cpu_seconds_total{cpu="0"} offset 1h
```
Combine a range vector selector with an offset modifier:
```
node_cpu_seconds_total{cpu="0"}[5m] offset 1h
```
